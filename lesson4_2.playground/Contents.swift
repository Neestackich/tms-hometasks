import UIKit

enum SomeEnumeration {
    case South
    case East
    case North
    case West
}

enum Currency: String {
    case dollar = "$"
    case euro = "E"
    case ruble = "R"
    case funt = "F"
}

let currentCurrency: Currency = Currency.dollar;
//можно так
let currenCurrency1: Currency = .dollar;
let currency: Currency = .euro;

switch currency {
    
case .dollar:
    print("Доллар");
case .euro:
    print("Евро")
case .ruble:
    print("Рубль");
case .funt:
    print("Фунт");
}

enum planet {
    case mercury, venus, earth, mars, jupiter,
     saturn, uranus
}


