import UIKit

let firstArray: Array<Int>;
let shortSyntaxArr: [Int];

let someInts = [Int]();
let someInts2: [Int] = [];

var shoppingList: Array<String> = ["Молоко", "Хлеб"];
print(shoppingList.count);

//сложение массива
var shopList2 = ["Шоколад", "Кофе"];

var result = shoppingList + shopList2;
print(result);

shoppingList += shopList2;

shoppingList.append("Помидоры");
shoppingList.append(contentsOf: shopList2);

var animals: [String] = ["Змея", "Собака", "Котик"]
print(animals[1]);

animals.insert("Супер-котик", at: 3);
animals.contains("Обезъянка");
animals.isEmpty;

var letters: Set<String> = [];
letters.insert("aaa");
letters.insert("bbb");
letters.contains("ccc");

var currency: [String: String] = [:]
var currency1: [String: String] = ["RUB": "Руб"];
var currency2: Dictionary<String, String>;

print(currency1["RUB"]!);
currency["RUB"] = "P";
currency["EUR"] = "Э";
print(currency);

currency.removeValue(forKey: "RUB");
currency["EUR"] = nil;

//кортежи

var schedule: [(hour: Int, description: String)] = [];
schedule.append((hour: 9, description: "Завтак"));
schedule.append((13, "dinner"));
//print(schedule.last!.hour, schedule[1]!.description)

let dinner: (hour: Int, description: String, place: String) = (18, "Ужин", "Омнамо");

for product in shoppingList {
    print("Продукт \(product)");
}

var variable: Int = 0;
