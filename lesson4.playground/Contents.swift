import UIKit

func printHello() -> () {
    print("Hello world");
}

printHello();

func sum(_ a: Int, _ b: Int) -> Int {
    return a + b;
}

print(sum(2, 4));

func print(printed text: String) {
    print(text);
}

print(printed: "Anna");

let numbers: [Int] = [1, 2, 3, 4, 5, 6, 7];

func geMminValue(_ array: [Int]) -> Int {
    return array.min()!;
}

func getValue(_ number: Int) -> Int {
    return number;
    
}

getValue(12);

func getValueFromArg(from number: Int) -> Void {
    print(number);
    print("from - ярлык")
    print("number - имя аргумента")
}

getValueFromArg(from: 12);

func anotherGetValue(number: Int) -> () {
    print(number);
}

anotherGetValue(number: 4);

func printNum(_ number: Int = 10) -> () {
    print(number);
}

printNum();


